#include <iostream>
#include <string>

using namespace std;

int editdistance(string str1, string str2);
int min(int a,int b,int c);
int hammingdistance(string str1,string str2);

int main(){
  string str1,str2;
  cout << "First:";
  cin >> str1;
  cout<< "Second:";
  cin >> str2;
  cout << "Edit distance: "<< editdistance(str1,str2)<<endl;
  cout << "Hamming distance:"<< hammingdistance(str1,str2)<<endl;
}

int min(int a,int b,int c){
  if (a<=b && a<=c)
    return a;
  if (b<=a && b<=c)
    return b;
  else
    return c;
}


int editdistance(string str1, string str2){
  int m= str1.length();
  int n = str2.length();

  int A[m+1][n+1];

  for (int i=0;i<=m;i++)
    A[i][0]= i;


  for (int i=0;i<=n;i++)
    A[0][i]= i;

  for(int i= 1;i<=m;i++){
    for(int j=1;j<=n;j++){
      if (str1[i-1]==str2[j-1])
	A[i][j] = A[i-1][j-1];
      else {
	A[i][j] = 1 + min(A[i-1][j-1],A[i-1][j],A[i][j-1]);
      }
    }
  }

  return A[m][n];
}


int hammingdistance(string str1,string str2){

  int h=0;
  for (int i= 0;i<str1.length();i++){
    if (str1[i]!=str2[i]) h++;

  }
  return h;
}
