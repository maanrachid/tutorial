#include <iostream>
#include <thread>
#include <mutex>
#include <chrono>


using namespace std;


mutex mtx;
int i=0;

void myMethod () {
  mtx.lock();
  /*
  cout<< "values are ::";
  for (int z=0; z< 10; ++z) {
    cout<< z;
    cout<< '\n';
  }
  cout<< "End of output !!"<<endl;
  */
  int z=i;
  z = z+1;
  this_thread::sleep_for (chrono::seconds(1));
  i = z;
  mtx.unlock();
}


int main ()
{
  cout<< "Demo for Mutex in C++"<<endl;;
  // cretaingtherad here
  thread thread1 (myMethod);
  thread thread2 (myMethod);
  thread thread3 (myMethod);
  // therad
  thread1.join();
  thread2.join();
  thread3.join();
  cout << i <<endl;
  return 0;
}
