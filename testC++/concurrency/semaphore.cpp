// Compile : g++ -lpthread semaphore.cpp
#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>

sem_t mutex;
int i=0;


void* thread(void* arg)
{
  //wait

  printf("%ld\n",(long)arg);

  sem_wait(&mutex);
  printf("\nEntered..\n");

  //critical section
  sleep(1);
  i++;
  //printf("%d\n",(long)arg);

  //signal
  printf("\nJust Exiting...\n");
  sem_post(&mutex);
}


int main()
{
  sem_init(&mutex, 0, 1);
  pthread_t t1,t2,t3,t4;
  long id;
  id=1;
  pthread_create(&t1,NULL,thread, reinterpret_cast<void*> (id));
  id=2;
  pthread_create(&t2,NULL,thread,  reinterpret_cast<void*> (id));
  id=3;
  pthread_create(&t3,NULL,thread, reinterpret_cast<void*> (id));
  id=4;
  pthread_create(&t4,NULL,thread, reinterpret_cast<void*> (id));
  pthread_join(t1,NULL);
  pthread_join(t2,NULL);
  pthread_join(t3,NULL);
  pthread_join(t4,NULL);
  sem_destroy(&mutex);
  return 0;
}
