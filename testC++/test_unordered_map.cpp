#include <unordered_map>
#include <unordered_set>
#include <iostream>
#include <string>


using namespace std;


int main(){

  unordered_map<string,int> m1 {
    {"hello",100},
      {"hi",200}
  };

  unordered_set<int> uset1;

  uset1.insert(10);
  uset1.insert(100);
  uset1.insert(300);
  uset1.erase(10);

  cout << "unordered set:"<<endl<<"------------"<<endl;

  if (uset1.find(10)!=uset1.end())
    cout << "10 is in the set"<<endl;

  for(auto i : uset1)
    cout << i <<endl;

  cout <<"Size:"<< uset1.size()<<endl;
  cout << "unordered map:"<<endl<<"------------"<<endl;


  cout <<m1["hello"]<<endl;
  if (m1.find("hello")!=m1.end())
    cout<<"hello is in the table!"<<endl;

  m1["maan"]=20;
  m1["mazen"]=30;
  m1.erase("hi");
  //  m1.push_back("maan",32);
  //m1.pop_front("mazen",33);


  for(auto i : m1){
    cout <<i.first<< " "<< i.second<<endl;
  }


  cout <<"Size:"<<m1.size()<<endl;

}

