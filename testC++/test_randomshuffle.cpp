#include <iostream>
#include <algorithm>
#include <cassert>
#include <functional>  
using namespace std;

int main() {

  int a[10];
  int i;
  for (i = 0; i < 10; ++i) 
    a[i] = i;

  random_shuffle(&a[0], &a[1],rand());

  for (i = 0; i < 10; ++i) 
    cout <<  a[i] << " ";

 
  return 0;
}
