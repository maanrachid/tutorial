//  g++ -std=c++11 test_map_vector_list.cpp
#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <cstdlib>
#include <bitset>
#include <list>
#include <algorithm>

struct mynode{

  struct mynode * next;

  int key;
  mynode (){next=NULL; key=0;}
};



struct stack {
  void push(int key){mynode *temp = new mynode(); temp->next= head;temp->key=key; 
    head= temp;size++;}
  int pop(){if (size==0) return -1;mynode *temp= head; head= head->next; int key=temp->key; delete(temp)
											    ; size--;
    return key;}
  mynode *head;
  stack(){head=NULL;size=0;}
  int size;
};






using namespace std;

int factorial(int i);
bool isprime1(int n,int i);
bool isprime(int n);
mynode* reverse_linked(mynode *head);



int main(){
  stack st1;
  int *A=new int[30];
  int B[3][4]= {10,20,30,40,50,60,70,80,90,100,100,210};
  map<int,int> m1;
  map<string,int> m2;
  string greet= "hello";
  int A2d[7][9];
  bitset<32> bset2(20);
  bitset<32> bset1(100);
  list<int> l1;

  cout<<"long long: "<<sizeof( long long)<<" bytes"<<endl;

  cout <<"Stack:"<<endl;
  cout <<"------------"<<endl;
  st1.push(10);
  st1.push(20);
  st1.push(30);
  cout<<st1.pop()<<endl;
  cout<<st1.pop()<<endl;
  st1.push(50);
  cout<<st1.pop()<<endl;
  cout<<st1.pop()<<endl;
  cout<<st1.pop()<<endl;
  cout<<st1.pop()<<endl;


  cout <<"2Darray:"<<endl;
  cout <<"-------"<<endl;
  for(int i=0;i<7;i++){
    for (int j=0;j<9;j++){
      A2d[i][j]=i*j;
    }
  }
  cout <<A2d[5][3]<< endl;
  cout <<A2d[6][4]<< endl<<"-----------"<<endl;
  cout <<"B"<<B[2][3]<<endl;

  cout<< "List:"<<endl;
  cout << "--------"<<endl;
  l1.push_back(10);
  l1.push_back(20);

  for (auto& i : l1){
    i = i+1;
    cout << i <<endl;
  }


  auto result = std::find(l1.begin(), l1.end(), 11);

  if (result !=l1.end())
    cout << "11 is in the list"<<endl;


  cout <<"bit sets:"<<endl;
  cout <<"-----------"<<endl;
  cout << bset2<<endl;
  bset2[22]=1;
  cout << bset2<<endl;
  cout << bset1<<endl;
  cout << (bset2 | bset1)<<endl;
  cout << "count:"<<bset1.count()<<endl;
  cout <<"Random numbers:"<<endl;
  cout <<"--------------"<<endl;
  srand(10);
  A[20]= rand()%1000;
  cout <<A[20]<<endl;

  cout <<"Map stuff:"<<endl;
  cout <<"----------"<<endl;

  m1[1]=20;
  m1[39]=1231;
  m2["hi"]=12;
  m2["hello"]=18;


  for (auto i : m1){
    cout <<i.first<<":"<<i.second<<endl;
  }

  for (auto i : m2){
    cout <<i.first<<":"<<i.second<<endl;
  }

  //  for (map<string,int>::iterator itm2= m2.begin(); itm2!=m2.end();itm2++){
  //  cout<<itm2->first<<":"<<itm2->second<<endl;
  //}


  cout <<"String stuff :"<<endl;
  cout <<"----------"<<endl;
  cout << greet<<endl;
  for(int i=0;i<greet.length();i++){
    cout << greet[i]<<" ";
  }

  cout <<endl;
  cout << greet.substr(3,2)<<endl;
  cout << greet.substr(5,1)<<endl;
  cout<< greet.find("el",0)<<endl;
  cout << greet.replace(1,2,"jjj")<<endl;


  cout << "vector stuff:"<<endl;
  cout << "----------------"<<endl;
  vector <int> vec = {100,200,300,400};

  vec.push_back(300);


  cout <<"Traversing using an easy way:"<<endl;
  for (int value : vec){
    cout<< value<<endl;
  }


  
  cout << "Linked list:"<<endl;
  cout << "------------"<<endl;
  A[1]=10;
  mynode *n1 = new mynode();
  mynode *temp3 = n1;
  int counter=0;
  while (counter<10){
    temp3->key= counter++;
    if (counter==10) break;
    temp3->next = new mynode();
    temp3 = temp3->next;
  }
  temp3->next=NULL;


  
  mynode *temp = n1;
  while (temp != NULL){
    cout<<temp->key<<endl;
    temp = temp ->next;
  }
  mynode* temp2 = reverse_linked(n1);
  temp = temp2;
  cout <<"Reveresed linked list:";
  while (temp != NULL){
    cout<<temp->key<<endl;
    temp = temp ->next;
  }



  cout <<"Functions:"<<endl<<"--------"<<endl;
  cout<<"Factorial 5 : "<<factorial(5)<<endl;
  for (int i=1;i<10;i++){
    cout << "Is prime "<< i <<":"<< isprime(i) <<endl;
  }
}


int factorial(int n){
  if (n<=1)
    return n;
  else 
    return n* factorial(n-1);

}


bool isprime(int n){
  return isprime1(n,n-1);
}


bool isprime1(int n,int i){
 

  if (i<=1)
    return true;

  if (n%i==0)
    return false;
  else 
    return isprime1(n,i-1);
}

mynode* reverse_linked(mynode *head){
  mynode * current = head;
  mynode * new_next= NULL;
  
  while (current!=NULL){
    mynode* temp = current->next;
    current->next = new_next;
    new_next = current;
    current = temp;
  }

  return new_next;
}
