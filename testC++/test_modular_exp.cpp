// downloaded from http://www.geeksforgeeks.org/modular-exponentiation-power-in-modular-arithmetic/

// Iterative C program to compute modular power
#include <stdio.h>
 
/* Iterative Function to calculate (x^y)%p in O(log y) */
int power(int x, unsigned int y, int p)
{
    int res = 1;      // Initialize result
 
    x = x % p;  // Update x if it is more than or 
                // equal to p
 
    while (y > 0)
    {
        // If y is odd, multiply x with result
        if (y & 1)
            res = (res*x) % p;
 
        // y must be even now
        y = y>>1; // y = y/2
        x = (x*x) % p;  
    }
    return res;
}
 
// Driver program to test above functions
int main()
{
   int x = 100;
   int y = 1000;
   int p = 499;
   for(int i=0;i<5000000;i++)
     power(x,y,p);
   printf("Power is %d\n", power(x, y, p));
   return 0;
}

