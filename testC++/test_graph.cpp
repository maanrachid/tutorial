#include <iostream>
#include <list>
using namespace std;

struct mynode2{
  struct mynode2 *next;
  int key;
};


void traverseB(mynode2 **graph, int nodes, bool visit[],bool tranv[]);
void traverseD(mynode2 **graph, int nodes, bool visited[]);
void traverseD_withstack(mynode2 **graph, int nodes, bool visited[]);


int main(){
  int nodes, edges;
  mynode2 **heads;
  cout <<"Enter number of nodes:";
  cin>> nodes;
  cout <<"Enter number of edges:";
  cin >> edges;

  heads = new mynode2 *[nodes];
  for(int i=0;i<nodes;i++){
    heads[i]=NULL;
  }


  for(int i=0;i<edges;i++){
    int v1, v2;
    cout <<"Enter edge: ";
    cin >> v1 >> v2;
    if (heads[v1]==NULL){
      heads[v1]= new mynode2();
      heads[v1]->key = v2;
    } else {
      mynode2 *temp = heads[v1];
      while (temp->next!=NULL)
	temp = temp->next;
      temp->next= new mynode2();
      temp->next->key = v2;
      temp->next->next = NULL;
    }

    if (heads[v2]==NULL){
      heads[v2]= new mynode2();
      heads[v2]->key = v1;
    } else {
      mynode2 *temp = heads[v2];
      while (temp->next!=NULL)
        temp = temp->next;
      temp->next= new mynode2();
      temp->next->key = v1;
      temp->next->next = NULL;
    }
  }

  for(int i=0;i<nodes;i++){
    mynode2 *temp= heads[i];
    cout <<"List "<<i<<":";
    while (temp!=NULL){
      cout<<temp->key<< " ";
      temp=temp->next;
    }
    cout <<endl;
  }


  bool *visit = new bool[nodes];
  bool *traversed = new bool[nodes];

  cout<<"Breadth-first search:";
  traverseB(heads,0,visit,traversed);
  cout <<endl;
  cout <<"Depth-first search:";
  visit = new bool[nodes]; 
  traverseD(heads,0, visit);
  delete [] visit;
  visit = new bool[nodes];
  cout<<endl;
  traverseD_withstack(heads,0, visit);
  cout<<endl;
  return 0;
  
}



void traverseD(mynode2 **graph,int noden,bool visited[]){
  mynode2 *temp = graph[noden];
  cout<< noden << " ";
  visited[noden]=true;

  while (temp!=NULL){
      int k = temp->key;
      if (!visited[k])
	traverseD(graph,k,visited);
      temp=temp->next;
   }
}


void traverseB(mynode2 **graph,int noden,bool visited[],bool traversed[]){
  list<int> l;
  cout<<"with a queue:";
  int v = 0;
  l.push_back(v);

  while (!l.empty()){
    int k = l.front();
    l.pop_front();
    if (!visited[k]){
      visited[k]=true;
      cout << k << " ";
    }
    mynode2 *temp = graph[k];
    while (temp !=NULL){
      if (!visited[temp->key])
        l.push_back(temp->key);
      temp = temp->next;
    }
  }

  cout <<endl;




}

void traverseD_withstack(mynode2 **graph, int nodes, bool visited[]){
  list<int> l;
  cout<<"Depth Search with a stack:";
  int v = 0;
  l.push_back(v);

  while (!l.empty()){
    int k = l.back();
    l.pop_back();
    if (!visited[k]){
      visited[k]=true;
      cout << k << " ";
    }
    mynode2 *temp = graph[k];
    while (temp !=NULL){
      if (!visited[temp->key])
	l.push_back(temp->key);
      temp = temp->next;
    }
  }

  cout <<endl;


}


