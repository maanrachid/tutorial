#include <iostream>


int main(){

  std::cout<<(static_cast<unsigned long long>(1) << 60)  <<std::endl; //shift 5 right to most significant bit
  std::cout<<(static_cast<unsigned long long>(32)>>3)<<std::endl; // shift 3 left to least significant bit
}
