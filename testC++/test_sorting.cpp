#include <iostream>
using namespace std;


void quicksort(int A[], int left, int right);
void validatesorting(const int A[],int count);
void swap(int A[],int i,int j);

int partition(int A[],int left, int right);
int binarysearch (int A[],int left, int right, int k);
void mergesort(int A[] , int left, int right);
void merge( int A[],int left, int mid, int right);



int main(){
  int count;

  cout<< "Enter the number of numbers:";
  cin >> count;

  int *A = new int [count];
  srand(time(NULL));
  
  for (int i=0;i<count;i++){
    A[i]= rand()%1000000;
    //cout<<A[i]<<endl;
  }
  //cout <<"----------"<<endl;
  
  quicksort(A,0,count-1);

  
  for (int i=0;i<count;i++){
    //cout<<A[i]<<endl;
    }
  cout << "validating quicksort:";
  validatesorting(A,count);

  for (int i=0;i<count;i++){
    A[i]= rand()%1000000;
    //cout<<A[i]<<endl;
  }

  mergesort(A,0,count);
  cout << "validating mergesort:";
  validatesorting(A,count);



  int B[8]={1,3,5,6,10,20,30,35};

  cout <<"Array: {1,3,5,6,10,20,30,35}"<<endl;
  
  for (int i=0;i<40;i++){
    int y = binarysearch (B,0, 7, i);
    if (y==-1)
      cout <<i <<  " Not found!"<<endl;
    else
      cout <<i << " Found in "<< y <<endl;
  }

}



void validatesorting(const int A[],int count){
  bool res = false;
  for (int i=0;i<count-1;i++){
    if (A[i]> A[i+1]){
      cout <<"Sorting Failed"<<endl;
      cout << A[i]<<">"<<A[i+1]<<endl;
      res =true;
      break;
    }
  }

  if (!res) cout << "Sorting ok!!"<<endl  ;
}



void quicksort(int A[], int left, int right){
  if (left<right){
    int p = partition(A,left,right);
    quicksort(A,left,p-1);
    quicksort(A,p+1,right);
  }
}

int partition(int A[],int left, int right){
  int pivot = A[right];
  int i= left-1;// where the boundry will be

  for(int j=left;j<right;j++){
    if (A[j]<pivot){
      i++;
      swap(A,i,j);
    }
  }
  swap(A,i+1,right);

  //  cout <<"<<<<<<<<<<<"<<pivot<<" "<<i+1<<endl;
  /*
for (int i=0;i<10;i++){
    cout<<A[i]<<endl;
  }
  cout <<"<<<<<<<<<<<"<<endl;
  */

  return i+1;
}


void swap(int A[],int i,int j){
  int temp = A[i];
  A[i] = A[j];
  A[j]=temp;
}


int binarysearch (int A[],int left, int right, int k){
  if (right>=left){
    int mid;
    mid = (right + left)/2;
    if (A[mid]==k) return mid;

    if (A[mid]>k) 
      return binarysearch(A,left,mid-1,k);

    //if (A[mid]<k)
    return binarysearch(A,mid+1,right,k);
  }

  return -1;
}




void mergesort(int A[] , int left, int right){
  if (right<=left) return;

  int mid = (right+left)/2;
  mergesort(A,left,mid);
  mergesort(A,mid+1,right);
  merge(A,left,mid,right);

}

void merge( int A[],int left, int mid, int right){

  int n1 = mid-left+1;
  int n2 = right-mid;


  int *arr1 = new int[n1];
  int *arr2 = new int [n2];

  for(int i=0;i<n1;i++)
    arr1[i]= A[left+i];

  for(int i=0;i<n2;i++)
    arr2[i]= A[mid+i+1];

  int p1 = 0;
  int p2 = 0;
  int k = left;

  while (p1<n1 && p2<n2){
    if (arr1[p1]>arr2[p2]){
      A[k]= arr2[p2];
      p2++;
    } else {
      A[k] = arr1[p1];
      p1++;
    }
    k++;
  }

  while (p1<n1){
    A[k]=arr1[p1];
    k++;
    p1++;
  }

  while (p2<n2){
    A[k]=arr2[p2];
    k++;
    p2++;
  }
  delete []  arr1;
  delete [] arr2;
}

