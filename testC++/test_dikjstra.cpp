#include <iostream>
#include <string>
#define SIZE 6
#define INF 100000

using namespace std;

int dij(int graph[SIZE][SIZE],int src);
int minvertex(const int distances[],const bool visited[]);


int main(){
  int graph[SIZE][SIZE]={
                   {0, 6, 7, 2, 3, 3},
		   {6, 0, 4, 5, 1, 2},
		   {7, 4, 0, 2, 3, 2},
		   {2, 5, 2, 0, 2, 2},
		   {3, 1, 3, 2, 0, 1},
		   {3, 2, 2, 2, 1, 0}};
  dij(graph,0);
  return 0;
}


int dij(int graph[SIZE][SIZE],int src){

  bool visited[SIZE];
  int distances[SIZE];


  // all distances are inf and all nodes are not visited
  for(int i=0;i<SIZE;i++){
    visited[i]= false;
    distances[i]= INF;
  }

  // starting nodes
  distances[src]=0;

  for(int i=0;i<SIZE;i++){
    int m= minvertex(distances,visited);
    cout <<"select "<<m<<endl;
    visited[m]=true;
    for(int j=0;j<SIZE;j++){
      if (!visited[j] and distances[j]>distances[m]+graph[m][j])
	distances[j]= distances[m]+graph[m][j];
    }
  }
  
  cout<<"Vertex\t\tDistance from source vertex"<<endl;
  for(int k = 0; k<SIZE; k++)                      
    { 
      char str=65+k; 
      cout<<str<<"\t\t\t"<<distances[k]<<endl;
    }
    return 0;
}


int minvertex(const int distances[],const bool visited[]){

  int min= INF;
  int index=0;
  for(int i=0;i<SIZE;i++){
    if (!visited[i] and  distances[i]<=min){
      min = distances[i];
      index=i;
    }
  }

  return index;


}
