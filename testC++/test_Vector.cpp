// downloaded from http://www.cplusplus.com/reference/vector/vector/vector/
#include <iostream>
#include <vector>

int main ()
{
  // constructors used in the same order as described above:

  // the iterator constructor can also be used to construct from arrays:
  int myints[] = {16,2,77,29};
  std::vector<int> fifth (myints, myints + sizeof(myints) / sizeof(int) );

  std::cout << "The contents of fifth are:";
  for (std::vector<int>::iterator it = fifth.begin(); it != fifth.end(); ++it)
    std::cout << ' ' << *it;
  std::cout << '\n';

  std::cout<<fifth[0]<<" "<<fifth[1]<<std::endl;
  return 0;
}
