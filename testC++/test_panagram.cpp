#include <iostream>
#include <string>

using namespace std;


bool is_pangram(string s){
  int counters[26];
  for (int i=0;i<26;i++) counters[i]=0;

  for (int i=0;i<s.length();i++){
    char c=s[i];
    if (c>=65 &&  c<=90)
	counters[c-65]++;
    else if (c>=97 && c<=122)
	counters[c-97]++;
    
  }

  for (int i=0;i<26;i++)  
    if (counters[i]==0)
      return false;
  
  return true;
}



int main(){
  string s;
  cout << "Enter your sentence:";
  getline(cin,s);
  if (is_pangram(s))
    cout<< "Yes, it is"<<endl;
  else
    cout<<"No, it is not"<<endl;
}
