#include <stdio.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

int main(){

  int *x,*y;
  x = reinterpret_cast<int *> (malloc(50000000000));
  if (x==NULL){
    cout<<"malloc fail!!"<<endl;
    return 0;
  }


  free(x);
  x=reinterpret_cast<int *>(malloc(10*4));
  y= x;
  for(int i=0;i<10;i++)
    y[i]=i;

  x = &y[3];

  for(int i=0;i<7;i++)
    printf("%d\n",x[i]);
 
  return 0;

}
