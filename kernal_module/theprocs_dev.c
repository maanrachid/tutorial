
#include<linux/sched.h>
#include <linux/string.h>
#include <linux/proc_fs.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/miscdevice.h>
#include <linux/module.h>
#include <asm/uaccess.h>
#include <linux/slab.h>


static ssize_t theprocs_read(struct file * file, char * buf, 
			     size_t count, loff_t *ppos)
{

  struct task_struct *task;
  
  char *procs_str = kmalloc(100000,GFP_KERNEL);
  int counter=0;

  for_each_process(task)
  {
    char small[50];
    int size = sprintf(small,"%s [%d]\n",task->comm, task->pid);
    int i;
    for(i=0;i<size;i++)
      procs_str[counter++]= small[i];
        
  }

  int len = counter;

  if (count < len)
    return -EINVAL;
  /*
   * If file position is non-zero, then assume the string has
   * been read and indicate there is no more data to be read.
   */
  if (*ppos != 0)
    return 0;
  /*
   * Besides copying the string to the user provided buffer,
   * this function also checks that the user has permission to
   * write to the buffer, that it is mapped, etc.
   */
  if (copy_to_user(buf, procs_str, len))
    return -EINVAL;
  /*
   * Tell the user how much data we wrote.
   */
  *ppos = len;
  kfree(procs_str);
  return len;
}

/*
 * The only file operation we care about is read.
 */

static const struct file_operations theprocs_fops = {
  .owner		= THIS_MODULE,
  .read		=  theprocs_read,
};

static struct miscdevice theprocs_dev = {
  /*
   * We don't care what minor number we end up with, so tell the
   * kernel to just pick one.
   */
  MISC_DYNAMIC_MINOR,
  /*
   * Name ourselves /dev/theprocs.
   */
  "theprocs",
  /*
   * What functions to call when a program performs file
   * operations on the device.
   */
  &theprocs_fops
};

static int __init
theprocs_init(void)
{
  int ret;

  /*
   * Create the "theprocs" device in the /sys/class/misc directory.
   * Udev will automatically create the /dev/theprocs device using
   * the default rules.
   */
  ret = misc_register(&theprocs_dev);
  if (ret)
    printk(KERN_ERR
	   "Unable to register the block device");
  

  return ret;
}

module_init(theprocs_init);

static void __exit
theprocs_exit(void)
{
  misc_deregister(&theprocs_dev);
}

module_exit(theprocs_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Maan Rachid");
MODULE_DESCRIPTION("Create List of Processes");
MODULE_VERSION("dev");
